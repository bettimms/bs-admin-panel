/**
 * Created by bettimms on 12/24/15.
 */

var http = require("http");
var express = require("express");

var app = express();
app.use(express.static(__dirname+"/admin/content"));

var server = http.createServer(app);
server.listen(3000);