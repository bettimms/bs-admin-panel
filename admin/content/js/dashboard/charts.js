/**
 * Created by bettimms on 12/9/15.
 */

(function ()
{
    /*These lines are all chart setup.  Pick and choose which chart features you want to utilize. */
    nv.addGraph(function ()
    {
        var chart = nv.models.lineChart()
            .margin({left: 100})  //Adjust chart margins to give the x-axis some breathing room.
            .useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
            .options({
                transitionDuration: 300,    // This should be duration: 300
                useInteractiveGuideline: true
            })  //how fast do you want the lines to transition?
            .showLegend(true)       //Show the legend, allowing users to turn on/off line series.
            .showYAxis(true)        //Show the y-axis
            .showXAxis(true);       //Show the x-axis


        chart.xAxis     //Chart x-axis settings
            .axisLabel('Time (ms)')
            .tickFormat(d3.format(',r'));

        chart.yAxis     //Chart y-axis settings
            .axisLabel('Stream')
            .tickFormat(d3.format('.02f'));

        /* Done setting the chart up? Time to render it!*/
        var myData = sinAndCos();   //You need data...

        d3.select('#line-chart svg')    //Select the <svg> element you want to render the chart in.
            .datum(myData)         //Populate the <svg> element with chart data...
            .call(chart);          //Finally, render the chart!

        //Update the chart when window resizes.
        nv.utils.windowResize(function ()
        {
            chart.update()
        });
        return chart;
    });
    /**************************************
     * Simple test data generator
     */
    function sinAndCos()
    {
        var sin = [], sin2 = [],
            cos = [];

        //Data is represented as an array of {x,y} pairs.
        for (var i = 0; i < 100; i++)
        {
            sin.push({x: i, y: Math.sin(i / 10)});
            sin2.push({x: i, y: Math.sin(i / 10) * 0.25 + 0.5});
            cos.push({x: i, y: .5 * Math.cos(i / 10)});
        }

        //Line chart data should be sent as an array of series objects.
        return [
            {
                values: sin,      //values - represents the array of {x,y} data points
                key: 'Sine Wave', //key  - the name of the series.
                color: '#ff7f0e'  //color - optional: choose your own line color.
            },
            {
                values: cos,
                key: 'Cosine Wave',
                color: '#2ca02c'
            },
            {
                values: sin2,
                key: 'Another sine wave',
                color: '#7777ff',
                area: true      //area - set to true if you want this line to turn into a filled area chart.
            }
        ];
    }


}());


//Bar chart
(function ()
{
    nv.addGraph(function ()
    {
        var chart = nv.models.discreteBarChart()
            .x(function (d)
            {
                return d.label
            })    //Specify the data accessors.
            .y(function (d)
            {
                return d.value
            })
            .staggerLabels(true)    //Too many bars and not enough room? Try staggering labels.
            .tooltips(true)        //Don't show tooltips
            .showValues(true)       //...instead, show the bar value right on top of each bar.
            .options({
                transitionDuration: 8300,    // This should be duration: 300
                useInteractiveGuideline: true
            })
            ;

        d3.select('#bar-chart svg')
            .datum(exampleData())
            .call(chart);

        nv.utils.windowResize(chart.update);

        return chart;
    });

//Each bar represents a single discrete quantity.
    function exampleData()
    {
        return [
            {
                key: "Cumulative Return",
                values: [
                    {
                        "label": "Mon",
                        "value": 29.765957771107
                    },
                    {
                        "label": "Tue",
                        "value": 5.5
                    },
                    {
                        "label": "Wed",
                        "value": 32.807804682612
                    },
                    {
                        "label": "Thur",
                        "value": 196.45946739256
                    },
                    {
                        "label": "Fri",
                        "value": 0.19434030906893
                    },
                    {
                        "label": "Sat",
                        "value": 98.079782601442
                    },
                    {
                        "label": "Sun",
                        "value": 13.925743130903
                    }
                ]
            }
        ]

    }


}());






//Pie chart
(function ()
{

    var data = exampleData();
    //Regular pie chart example
    nv.addGraph(function ()
    {
        var svg = d3.select('#bar-chart-small svg');

        //var legend = nv.models.legend();
        var chart = nv.models.pieChart()
            .x(function (d)
            {
                return d.label
            })
            .y(function (d)
            {
                return d.value
            })
            .growOnHover(true)
            .legendPosition("right")
            .margin({"left": 1, "right": 1, "top": 1, "bottom": 1})
            .showLegend(true)
            .showLabels(false);

        d3.select("#bar-chart-small svg")
            .datum(exampleData())
            .transition().duration(350)
            .call(chart);

        function updateLegendPosition() {
            svg.selectAll(".nv-series")[0].forEach(function(d, i)
            {
                var verticalOffset = 1.5;
                d3.select(d).attr("transform", "translate(30," + (i+verticalOffset) * 20 + ")");
            })
        }
        svg.select('.nv-legend').on("click", function() {
            updateLegendPosition();
        });

        svg.select('.nv-legend').on("dblclick", function() {
            updateLegendPosition();
        });

        updateLegendPosition();

        //Scale pieChart
        d3.selectAll(".nv-pieWrap")
            .attr("transform","translate(0,-13) scale(1.178)");
        updateLegendPosition();

        return chart;
    });

//Pie chart example data. Note how there is only a single array of key-value pairs.
    function exampleData()
    {
        return [
            {
                "label": "One",
                "value": 29.765957771107
            },
            {
                "label": "Two",
                "value": 0
            },
            {
                "label": "Three",
                "value": 32.807804682612
            },
            {
                "label": "Four",
                "value": 196.45946739256
            }
        ];
    }
});


//C3 bar chart small
(function(){
    var chart = c3.generate({
        bindto: '#bar-chart-small',
        data: {
            columns: [
                ['data1', 30, 20, 50, 40, 60, 50],
                ['data2', 200, 130, 90, 240, 130, 220],
                ['data3', 300, 200, 160, 400, 250, 250],
                ['data4', 300, 200, 160, 400, 250, 250],
            ],
            type: 'bar',
            types:{data4: 'line'},
            colors: {
                data1: '#009ECE',
                data2: '#9CCF31',
                data3: '#F2635F',
                data4: 'magenta'
            },
            color: function (color, d) {
                // d will be 'id' when called for legends
                return d.id && d.id === 'data3' ? color : color;
            }
        }
    });
})();

//C3 pie chart
(function ()
{
    var chart = c3.generate({
        bindto:"#pie-chart-small",
        data: {
            // iris data from R
            columns: [
                ['data1', 30],
                ['data2', 120],
            ],
            type : 'pie',
            onclick: function (d, i) {  },
            onmouseover: function (d, i) {  },
            onmouseout: function (d, i) {    }
        },
        legend:{
            position:"right"
        }
    });

    setTimeout(function () {
        chart.load({
            columns: [
                ["setosa", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
                ["versicolor", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
                ["virginica", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
            ]
        });
    }, 1500);

    setTimeout(function () {
        chart.unload({
            ids: 'data1'
        });
        chart.unload({
            ids: 'data2'
        });
    }, 2500);
})();



(function ()
{
    require.config({
        paths: {
            echarts: 'http://echarts.baidu.com/build/dist'
        }
    });

    // use
    require(
        [
            'echarts',
            'echarts/chart/bar' // require the specific chart type
        ],
        function (ec)
        {
            // Initialize after dom ready
            var myChart = ec.init(document.getElementById('bar-chart-small'));

            option = {
                title: {
                    text: 'ECharts2 vs ECharts1',
                    subtext: 'Chrome'
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: [
                        'ECharts1', 'ECharts1', 'ECharts1',
                        'ECharts2', 'ECharts2', 'ECharts2'
                    ]
                },
                toolbox: {
                    show: true,
                    feature: {
                        mark: {show: false},
                        dataView: {show: true, readOnly: false},
                        magicType: {show: true, type: ['line', 'bar']},
                        restore: {show: true},
                        saveAsImage: {show: true}
                    }
                },
                calculable: true,
                grid: {y: 70, y2: 30, x2: 20},
                xAxis: [
                    {
                        type: 'category',
                        data: ['Line', 'Bar', 'Scatter', 'K', 'Map']
                    },
                    {
                        type: 'category',
                        axisLine: {show: false},
                        axisTick: {show: false},
                        axisLabel: {show: false},
                        splitArea: {show: false},
                        splitLine: {show: false},
                        data: ['Line', 'Bar', 'Scatter', 'K', 'Map']
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        axisLabel: {formatter: '{value} ms'}
                    }
                ],
                series: [
                    {
                        name: 'ECharts2',
                        type: 'bar',
                        itemStyle: {normal: {color: 'rgba(193,35,43,1)', label: {show: false}}},
                        data: [40, 155, 95, 75, 0]
                    },
                    {
                        name: 'ECharts2',
                        type: 'bar',
                        itemStyle: {
                            normal: {
                                color: 'rgba(181,195,52,1)',
                                label: {show: false, textStyle: {color: '#27727B'}}
                            }
                        },
                        data: [100, 200, 105, 100, 156]
                    },
                    {
                        name: 'ECharts2',
                        type: 'bar',
                        itemStyle: {
                            normal: {
                                color: 'rgba(252,206,16,1)',
                                label: {show: false, textStyle: {color: '#E87C25'}}
                            }
                        },
                        data: [906, 911, 908, 778, 0]
                    },
                    {
                        name: 'ECharts1',
                        type: 'bar',
                        xAxisIndex: 1,
                        itemStyle: {
                            normal: {
                                color: 'rgba(193,35,43,0.5)', label: {
                                    show: false, formatter: function (p)
                                    {
                                        return p.value > 0 ? (p.value + '\n') : '';
                                    }
                                }
                            }
                        },
                        data: [96, 224, 164, 124, 0]
                    },
                    {
                        name: 'ECharts1',
                        type: 'bar',
                        xAxisIndex: 1,
                        itemStyle: {normal: {color: 'rgba(181,195,52,0.5)', label: {show: false}}},
                        data: [491, 2035, 389, 955, 347]
                    },
                    {
                        name: 'ECharts1',
                        type: 'bar',
                        xAxisIndex: 1,
                        itemStyle: {
                            normal: {
                                color: 'rgba(252,206,16,0.5)', label: {
                                    show: true, formatter: function (p)
                                    {
                                        return p.value > 0 ? (p.value + '+') : '';
                                    }
                                }
                            }
                        },
                        data: [3000, 3000, 2817, 3000, 0]
                    }
                ]
            };


            // Load data into the ECharts instance
            myChart.setOption(option);
        }
    );
});
(function ()
{
    require.config({
        paths: {
            echarts: 'http://echarts.baidu.com/build/dist'
        }
    });

    // use
    require(
        [
            'echarts',
            'echarts/chart/pie' // require the specific chart type
        ],
        function (ec)
        {
            // Initialize after dom ready
            var myChart = ec.init(document.getElementById('pie-chart-small'));

            option = {
                title: {
                    text: 'T',
                    subtext: 'E',
                    x: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    x: 'center',
                    y: 'bottom',
                    data: ['rose1', 'rose2', 'rose3', 'rose4', 'rose5', 'rose6', 'rose7', 'rose8']
                },
                toolbox: {
                    show: true,
                    feature: {
                        mark: {show: true},
                        dataView: {show: true, readOnly: false},
                        magicType: {
                            show: true,
                            type: ['pie', 'funnel']
                        },
                        restore: {show: true},
                        saveAsImage: {show: true}
                    }
                },
                calculable: true,
                series: [
                    {
                        name: '半径模式',
                        type: 'pie',
                        radius: [20, 110],
                        center: ['25%', 200],
                        roseType: 'radius',
                        width: '40%',       // for funnel
                        max: 40,            // for funnel
                        itemStyle: {
                            normal: {
                                label: {
                                    show: false
                                },
                                labelLine: {
                                    show: false
                                }
                            },
                            emphasis: {
                                label: {
                                    show: true
                                },
                                labelLine: {
                                    show: true
                                }
                            }
                        },
                        data: [
                            {value: 10, name: 'rose1'},
                            {value: 5, name: 'rose2'},
                            {value: 15, name: 'rose3'},
                            {value: 25, name: 'rose4'},
                            {value: 20, name: 'rose5'},
                            {value: 35, name: 'rose6'},
                            {value: 30, name: 'rose7'},
                            {value: 40, name: 'rose8'}
                        ]
                    }
                ]
            };


            // Load data into the ECharts instance
            myChart.setOption(option);
        }
    );
});